# Monitoring

Manuelles Überwachen ständig sterbender, neu gestarteter sowie veränderter Services, Container und sonstiger Ressourcen im Container-Cluster stellt komplett neue Anforderungen an das Monitoring. 

Setups wie in konventionellen Sysadmin-Umgebungen auf Blech oder VMs sind hier weder praktikabel oder sinnvoll noch in irgendeiner Form realisierbar.

### Installation

Die Installation des Open Source Monitoring Stack erfolgt am einfachsten mit `helm`

Dazu erstellen wir zuerst einen eigenen Namespace und verwenden dann das [Helm Chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack) von [Open Source Monitoring Stack](https://github.com/prometheus-operator/kube-prometheus)

    kubectl create namespace monitoring
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring

Der [Open Source Monitoring Stack](https://github.com/prometheus-operator/kube-prometheus) beinhaltet:

* Grafana
* Prometheus 
* Alertmanager 

Um auf die UI zugreifen zu können müssen wir die Ports der User Interfaces, von auf ClusterIP auf LoadBalancer ändern:

    kubectl -n monitoring patch service monitoring-grafana                      -p '{"spec": {"type": "LoadBalancer"}}'
    kubectl -n monitoring patch service monitoring-kube-prometheus-prometheus   -p '{"spec": {"type": "LoadBalancer"}}'
    kubectl -n monitoring patch service monitoring-kube-prometheus-alertmanager -p '{"spec": {"type": "LoadBalancer"}}'

Die weiter geleiteten Ports finden lassen sich durch die Ausgabe der Service herausfinden:

    kubectl -n monitoring get services

Grafana UI Einloggen mittels **User/Password admin/prom-operator**. Auf der linken Seite kann zwischen einer Reihe von vorbereitenden Dashboards ausgewählt werden, z.B. ../Cluster.

In der Prometheus Oberfläche kann mittels der Abfragesprache PromQL gezielt Ressourcen ausgewählt werden, z.B. durch Query von `apiserver_storage_objects`.

Der Alertmanager dient zum Verarbeiten von Warnungen. Für ein Beispiel siehe [Notifikation Beispiel](https://prometheus.io/docs/alerting/notification_examples/).

### Links

* [Prometheus](https://prometheus.io/)
* [Open Source Monitoring Stack](https://github.com/prometheus-operator/kube-prometheus)
* [Helm Chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack)
* [Vorbereitete Grafana Dashboards](https://github.com/kubernetes-monitoring/kubernetes-mixin)
* [Notification Beispiel](https://prometheus.io/docs/alerting/latest/notification_examples/)
* [Konfigurations Alertmanager](https://prometheus.io/docs/alerting/latest/configuration/) - zuerst in Container wechseln!
* [Beispiel Alertmeldung via Mail](https://github.com/blro-ep/ITCNE23-SEM-I/blob/main/monitoring/alertmanager/alertmanager.yml)

Hands-on aus Docker und Kubernetes Kurs

* [Metrics](https://github.com/mc-b/duk/blob/master/data/jupyter/demo/Monitoring-Metrics.ipynb)
* [Installation Monitoring Stack](https://github.com/mc-b/duk/blob/master/data/jupyter/demo/Monitoring-Cluster.ipynb)
* [Komplettes Beispiel mit eigenem Service](https://github.com/mc-b/duk/blob/master/data/jupyter/demo/Monitoring-Service.ipynb)

