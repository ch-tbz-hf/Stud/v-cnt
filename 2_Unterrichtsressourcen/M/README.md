# Serverless (Function as a Service)

Die Funktion als Dienst (FaaS) ist ein relativ neues Architekturmuster. 

Es entstand, als große Cloud-Anbieter wie AWS Produkte wie Lambda-Funktionen anboten, gefolgt von Azure-Funktionen (Microsoft Azure) und Google Cloud-Funktionen (Google Cloud). 

Die Idee hinter diesen Produkten ist, dass Sie manchmal keinen Service im "Always-On" -Modus benötigen. 

Sie möchten vielmehr eine „einmalige“ Art von Service. Er wird nur aktiviert, wenn eine Anfrage eintrifft und "stirbt" dann.

### Links

* [Knative](https://knative.dev/docs/)
* [Autoscaling aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/Serverless-Knative-Autoscaling.ipynb)
* [Serving aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/Serverless-Knative-Serving.ipynb)
* [Eventing aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/Serverless-Knative-Eventing.ipynb)
* [Sequence aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/Serverless-Knative-Sequence.ipynb)
