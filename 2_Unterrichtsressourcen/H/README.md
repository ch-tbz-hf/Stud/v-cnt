# Helm

Fast jede Programmiersprache und jedes Betriebssystem verfügt über einen eigenen Paketmanager, der bei der Installation und Wartung von Software hilft. 
Helm bietet die gleichen grundlegenden Funktionen wie viele der Paketmanager, mit denen Sie möglicherweise bereits vertraut sind, z. B. Debian apt oder Python pip.

Helm kann:
* Software installieren.
* Software-Abhängigkeiten automatisch auflösen und installieren.
* Software aktualisieren.
* Konfigurieren von Software (Paketen) in Repositories
* Abrufen von Softwarepaketen aus Repositories

### Beispiel

Im Verzeichnis [hello-chart](hello-chart/) befindet sich ein einfaches Beispiel. Es installiert den `hello-world` Services. 

Es funktioniert ohne HELM Repository und kann wie folgt installiert werden:

    helm install <name> ./hello-chart

Dabei ist zu beachten, dass HELM automatisch den Eintrag `appVersion` aus `values.yaml` hinter den Image Namen hängt.

Das Chart testen, ohne Installation

    helm install hello --dry-run ./hello-chart

Das gibt die erstellten YAML Dateien aus. Diese können, dann vorgängig, nochmals kontrolliert werden.    

### Links

* [Quickstart Guide](https://helm.sh/docs/intro/quickstart/)
* [Chart Template Guide](https://helm.sh/docs/chart_template_guide/getting_started/)
* [Homepage](https://helm.sh/)
* [Artifact Hub](https://artifacthub.io/)
* [Helm Beispiele (veraltet)](https://github.com/helm/charts)
