# Liveness-, Readiness- und Startup-Tests

**Liveness-Probes (dt: Lebendigkeitstests)** helfen, um zu wissen, wann ein Container neu gestartet werden muss. Zum Beispiel könnten Liveness-Probes einen Deadlock abfangen, wenn eine Anwendung ausgeführt wird, aber keine Fortschritte erzielen kann (z.B. abhängige Datenbank startet nicht).

**Startup-Probes (dt: Startstests)** helfen, um festzustellen, wann eine Containeranwendung gestartet wurde. Wenn eine solche Tests konfiguriert ist, werden die Lebendigkeits- und Bereitschaftsprüfungen deaktiviert, bis sie erfolgreich ist, um sicherzustellen, dass diese Tests den Anwendungsstart nicht stören.

**Readiness-Probes (dt: Bereitschaftstests)** helfen, um festzustellen, wann ein Container bereit ist, Datenverkehr anzunehmen. Ein Pod gilt als bereit, wenn alle seine Container bereit sind. Wenn ein Pod nicht bereit ist, wird er aus den Service Load Balancern entfernt.

### Links

* [Kubernetes Doku](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)
* [Hands-on aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk/blob/master/data/jupyter/09-9-Tests.ipynb)