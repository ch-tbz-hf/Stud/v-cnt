# RBAC

 Rollenbasierter Zugriff auf das Kubernetes-API.

 ### Links

* [Aktivierung RBAC in microk8s](https://microk8s.io/docs/multi-user)
* [Kubernetes Doku](https://kubernetes.io/docs/concepts/security/controlling-access/)

* [Hands-on aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/RBAC-User.ipynb)