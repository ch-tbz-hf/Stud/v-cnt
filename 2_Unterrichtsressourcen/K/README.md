# Logging

Mithilfe von Anwendungs- und Systemprotokollen können Sie besser verstehen, was in Ihrem Cluster geschieht.

Die Protokolle sind besonders nützlich zum Debuggen von Problemen und zum Überwachen der Clusteraktivität. 

Die einfachste und am weitesten verbreitete Protokollierungsmethode für containerisierte Anwendungen besteht darin, in die Standardausgabe (stdout) und die Standardfehlerströme (stderr) zu schreiben.

Wenn ein Container neu gestartet wird, behält Kubelet standardmässig einen beendeten Container mit seinen Protokollen bei. Wenn ein Pod aus dem Knoten entfernt wird, werden auch alle entsprechenden Container zusammen mit ihren Protokollen entfernt.

### Links

* [Fluentbit - leichtgewichtiges Logging System](https://docs.fluentbit.io/manual/installation/kubernetes/)
* [Hands-on aus Docker und Kubernetes Kurs](https://github.com/mc-b/duk-demo/blob/master/data/jupyter/demo/Logging.ipynb)


