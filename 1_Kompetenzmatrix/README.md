# Kompetenzmatrix - Modul V-CNT


| Wertung | Beschreibung |
|--- | ----- |
| 25 % | Theorie<br>- Ich kenne die in der Kompetenz erwähnte Technologie/Funktion<br>- Ich kann die Kompetenz, mit eigenen Worten, erklären<br>- Ich kann die Kompetenz anhand eines, eigenen, Beispiels erklären |
| 50 % | Praktische Umsetzung<br>- Ich habe die vorhandenen Beispiele durchgespielt<br>- Ich habe die Beispiele geändert/erweitert<br>- Ich habe eine eigene Implementierung, für meine Umgebung/Firma, umgesetzt |
| 25 % | Dokumentation<br>- Es ist eine Dokumentation vorhanden<br>- Es ist eine illustrierte Dokumentation im Markdown Format, in einem öffentlichen GitLab Repository, vorhanden<br>- Dritte können, anhand der Dokumentation, das Ganze Nachbauen |

**Bemerkungen**: Die Punkte gelten additiv, d.h. für eine volle Note sind alle Kriterien zu erfüllen.
Nr. 1 und 3 gibt 1 - 3 Punkte, Nr. 2 gibt 1 - 6 Punkte, Maximum 12 Punkte pro Kompetenz




