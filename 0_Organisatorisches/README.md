# Organisatorisches - V-CNT

## Autoren

| Name      | Vorname | Schule | Email                   |
| --------- | ------- | ------ | ----------------------- |
| Bernet    | Marcel  | TBZ    | marcel.bernet@tbz.ch    |